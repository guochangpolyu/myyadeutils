### add necessary modules
import json,sys
with open('/home/gc/.vscode-server/data/Machine/settings.json','r') as f:
    extroPaths=json.load(f)
for i in extroPaths['python.autoComplete.extraPaths']:
    sys.path.append(i)

import csv
import shutil
import os
from yade.utils import *
from yade import plot
from commonCodes.myCommonSavers import *
from yadeCodes.yadeSavers import *
### add code path in vscode
from myThingsClass import *

#### saver ####
def sumSaver(I:information,anythingSpecial:str='',spSwitch:bool=False):
    """save I(.data) and plot(.data,and .csv) into I.savePath+'/'+str(O.iter)+anythingSpecial 
    """
    path=I.savePath+'/'+str(O.iter)+anythingSpecial
    checkPath(path)
    O.save(path+'/yade.yade.bz2')
    print('*********yadefile, ',end='')
    plotSaver(path+'/PlotData.csv',path+'/PlotData.data')
    print('plot, ',end='')
    saver(I,path+'/I.data')
    print(f'information are saved in {path}')
    if spSwitch:
        saveIC(I,path)
    flushLog()

#### saver ####

def saveIC(I:information,ICAbsPath:str):
    """
    save particle center and radius after finishing the internal compression into ICAbsPath+'paSP.data'
    save pipe center, I.ppLongNum, I.ppOutDiameter, and I.ppRmax after finishing the internal compression into ICAbsPath+'ppSP.data'
    """
    paSpherePack=[]
    for bid in I.paID:
        x = O.bodies[bid].state.pos[0] % I.WidthX
        z = O.bodies[bid].state.pos[2] % I.HeightZ
        if  ((I.bdAabbMin[0]<x<I.bdAabbMax[0]) and (I.bdAabbMin[2]<z<I.bdAabbMax[2])):
            # print(O.bodies[bid].state.pos, O.bodies[bid].shape.radius)
            paSpherePack.append([O.bodies[bid].state.pos, O.bodies[bid].shape.radius])
    saver(paSpherePack,ICAbsPath+'/SPpa.data')
    ppCenters=[]
    for bid in I.ppMemberid:
        ppCenters.append(O.bodies[bid].state.pos)
    saver([ppCenters,I.ppLongNum,I.ppOutDiameter,I.ppRmax],ICAbsPath+'/SPpp.data')

def loadICParticle(I:information,ICAbsPath:str):
    """load the particle center and radius. using these information to append particles and give the I.paID
    """
    I.paID=[O.bodies.append(sphere(c,r,material=O.materials[I.paMID])) for c,r in reader(ICAbsPath+'paSP.data')]

def loadICPipe(I:information,ICAbsPath:str):
    """load the pipe center, I.ppLongNum, I.ppOutDiameter, and I.ppRmax. using these information to append pipe and give the I.ppClumpid, I.ppMemberid
    """
    [ppCenters,I.ppLongNum,I.ppOutDiameter,I.ppRmax]=reader(ICAbsPath+'ppSP.data')
    I.ppClumpid, I.ppMemberid = O.bodies.appendClumped(
        [sphere(center, radius=I.ppOutDiameter/2, color=(1, 1, 0),material=O.materials[I.ppMID]) for center in ppCenters])


