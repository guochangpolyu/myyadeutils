### add necessary modules
import json,sys
with open('/home/gc/.vscode-server/data/Machine/settings.json','r') as f:
    extroPaths=json.load(f)
for i in extroPaths['python.autoComplete.extraPaths']:
    sys.path.append(i)

import csv
import shutil
import os
from yade.utils import *
from yade import plot
from commonCodes.myCommonSavers import *
### add code path in vscode


def plotSaver(csvFileName:str,dataFileName:str=''):
    """save the plot csv and data files into csvFileName and dataFileName, respectively.

    Args:
        csvFileName (_type_): _description_
        dataFileName (_type_): _description_
    """
    with open(csvFileName,'w') as f:
        # pass the csv file to csv.writer.
        writer = csv.writer(f)
        # convert the dictionary keys to a list
        key_list = list(plot.data.keys())
        orginzeList(key_list,'i','t')
        # find the length of the key_list
        limit = len(plot.data['i'])
        # the length of the keys corresponds to no. of. columns.
        writer.writerow(key_list)
        # iterate each column and assign the corresponding values to the column
        for i in range(limit):
            writer.writerow([plot.data[x][i] for x in key_list])
    if dataFileName!='':
        saver(plot.data,dataFileName)
#### plot ####

def saveCode(targetPath:str=''):
    """save files in current folder to targetPath. and end of filename in noNeed will not be saved.
    noNeed = ['.npy', '.md', '.git', '.data', '.csv', '.log', '.gitignore', '.pdf','.yade','.yade.bz2','.xml','.xml.bz2','.txt']
    """
    noNeed = ['.npy', '.md', '.git', '.data', '.csv', '.log', '.gitignore', '.pdf','.yade','.yade.bz2','.xml','.xml.bz2','.txt']
    checkPath(targetPath)                
    for item in os.listdir():
        if os.path.isfile(item):
            if not any(item.endswith(nN) for nN in noNeed):
                shutil.copy(item, targetPath + item)
